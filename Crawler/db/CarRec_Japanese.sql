-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: localhost    Database: CarRec
-- ------------------------------------------------------
-- Server version	5.7.16-enterprise-commercial-advanced

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Japanese`
--

DROP TABLE IF EXISTS `Japanese`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Japanese` (
  `idJapanese` int(11) NOT NULL AUTO_INCREMENT,
  `Maker` varchar(45) DEFAULT NULL,
  `Model` varchar(45) DEFAULT NULL,
  `MinPrice` varchar(45) DEFAULT NULL,
  `MaxPrice` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idJapanese`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=big5;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Japanese`
--

LOCK TABLES `Japanese` WRITE;
/*!40000 ALTER TABLE `Japanese` DISABLE KEYS */;
INSERT INTO `Japanese` VALUES (1,'Toyota','Camry','21356','23935'),(2,'Toyota','Corolla','17414','19365'),(3,'Toyota','RAV4','23655','25850'),(4,'Toyota','Highlander','28383','31430'),(5,'Subaru ','Forester','21832','23470'),(6,'Subaru ','Impreza','18387','19215'),(7,'Subaru ','Legacy','21230','22815'),(8,'Subaru ','Outback','24678','26520'),(9,'Honda','CR-V','22205','24745'),(10,'Honda','Civic','17932','19575'),(11,'Honda','Accord','20287','23190');
/*!40000 ALTER TABLE `Japanese` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-02 10:01:50
