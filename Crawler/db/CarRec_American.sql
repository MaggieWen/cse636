-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: localhost    Database: CarRec
-- ------------------------------------------------------
-- Server version	5.7.16-enterprise-commercial-advanced

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `American`
--

DROP TABLE IF EXISTS `American`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `American` (
  `idAmerican` int(11) NOT NULL AUTO_INCREMENT,
  `Maker` varchar(45) DEFAULT NULL,
  `Model` varchar(45) DEFAULT NULL,
  `MinPrice` varchar(45) DEFAULT NULL,
  `MaxPrice` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idAmerican`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=big5;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `American`
--

LOCK TABLES `American` WRITE;
/*!40000 ALTER TABLE `American` DISABLE KEYS */;
INSERT INTO `American` VALUES (1,'Ford','Focus','17063','17650'),(2,'Ford','Fusion','21072','22995'),(3,'Ford','Mustang','24097','25545'),(4,'GMC','Acadia','28135','29995'),(5,'GMC','Savana','30776','33985'),(6,'GMC','Sierra','27519','28910'),(7,'Dodge','Challenger','26181','27990'),(8,'Dodge','Charger','26671','28990'),(9,'Dodge','Dart','16190','17990'),(10,'Buick','Regal','26223','27990'),(11,'Buick','Enclave','37601','39990'),(12,'Buick','LaCrosse','31545','32990');
/*!40000 ALTER TABLE `American` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-02 10:01:50
