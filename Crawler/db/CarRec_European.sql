-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: localhost    Database: CarRec
-- ------------------------------------------------------
-- Server version	5.7.16-enterprise-commercial-advanced

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `European`
--

DROP TABLE IF EXISTS `European`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `European` (
  `idEuropean` int(11) NOT NULL AUTO_INCREMENT,
  `Maker` varchar(45) DEFAULT NULL,
  `Model` varchar(45) DEFAULT NULL,
  `MinPrice` varchar(45) DEFAULT NULL,
  `MaxPrice` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEuropean`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=big5;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `European`
--

LOCK TABLES `European` WRITE;
/*!40000 ALTER TABLE `European` DISABLE KEYS */;
INSERT INTO `European` VALUES (1,'BMW','M3','58808','64195'),(2,'BMW','X3','36895','39945'),(3,'BMW','3 Series','30951','34445'),(4,'Audi','S3','40848','43850'),(5,'Audi','A3','29734','32150'),(6,'Audi','A4','34380','36195'),(7,'Audi','Q5','38314','41850'),(8,'Volvo','S60','29510','35090'),(9,'Volvo','S80','36563','44390'),(10,'Volvo','XC70','33483','38040'),(11,'Volve','XC90','44017','46745'),(12,'Volve','XC60','37414','41945'),(13,'Volkswagen','Golf','18563','19716'),(14,'Volkswagen','Jetta','16091','18715'),(15,'Volkswagen','Tiguan','22642','24323');
/*!40000 ALTER TABLE `European` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-02 10:01:50
