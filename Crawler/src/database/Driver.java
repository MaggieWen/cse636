package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map; 

public class Driver {
	String maker; 
	String model; 
	String price; 
	String MinPrice; 
	String MaxPrice;
	
	//which one should be the final result? 
	
	
	
	//American<Maker <Model  <Price <min, max>>> 
	ArrayList<HashMap<String, HashMap<String, String>>> NorthAmerican = new ArrayList<HashMap<String, HashMap<String, String>>>(); 
	ArrayList<HashMap<String, HashMap<String, String>>> European = new ArrayList<HashMap<String, HashMap<String, String>>>(); 
	ArrayList<HashMap<String, HashMap<String, String>>> Japanese = new ArrayList<HashMap<String, HashMap<String, String>>>(); 
	
	static final String JDBC_Driver = "com.mysql.jdbc.Driver"; 
	static final String url = "jdbc:mysql://localhost:3306/CarRec"; 
	static String user = "root"; 
	static  String password = "mysql"; 
	
	private  void createDB(){
		String createDB = "CREATE DATABASE CarRec; " + "USE CarRec; "; 
	}
	private  void createTable() {
		Connection dbConnection = null; 
		PreparedStatement stmt = null; 
		String createTable = "CREATE TABLE CarRec" + "(Maker VARCHAR(255), "
				+ "Model VARCHAR(255), " + "MinPrice VARCHAR(255), " + "MaxPrice VARCHAR(255)"; 
		
		try{
			dbConnection = getDBConnection();
			stmt = dbConnection.prepareStatement(createTable);
			stmt.execute(createTable); 
			System.out.println("Creating Table now......");
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		finally{
			try{
				if(stmt != null) dbConnection.close();
			}catch(SQLException e){
				System.out.println(e.getMessage());
			}
		}
		System.out.println("Successfully created the tabel");
	}

	private static void insertRecordIntoTable() throws SQLException{
		Connection dbConnection = null; 
		PreparedStatement stmt = null; 
		String insertTableSQL = "INSERT INTO carRecommendation (Maker, Model, MinPrice, MaxPrice) VALUES('Toyota', 'Camry', '24000', '28000')";
						
		try{
			dbConnection = getDBConnection();
			stmt = dbConnection.prepareStatement(insertTableSQL);
			
			stmt.execute(insertTableSQL); 
			System.out.println("Record is inserted into DB table"); 
		}
		catch(SQLException e){
			System.out.println(e.getMessage()); 
		}
		finally{
			if(stmt != null){
				stmt.close(); 
			}
			if(dbConnection != null){
				dbConnection.close(); 
			}
		}
		System.out.println("Successfully inserted data into the database.");
	}
	
	public  void initial(  String useraccount, String password){
		user=useraccount;
		this.password=password;
		
		createDB();
		
		
	}

	private static Connection getDBConnection() {
		Connection dbConnection = null; 
		try{
			Class.forName(JDBC_Driver); 
		}
		catch(ClassNotFoundException e){
			System.out.println(e.getMessage()); 
		}
		try{
			dbConnection = DriverManager.getConnection(url, user, password); 
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}
	
	public List<List<String>> retDB(String price, String range){
		
		 List<List<String>> finalResult = new ArrayList();
		 int defaultRange=2000;
		 if(range!=null) defaultRange=Integer.valueOf(range);
		 
		 int p=Integer.valueOf(price);
		 
		 int min=p-defaultRange;
		 int max=p+defaultRange;
		
		try{
			Connection myConn = DriverManager.getConnection(url, user, password);
			Statement myStmt = myConn.createStatement(); 
			ResultSet myRs = myStmt.executeQuery("select * from American WHERE MaxPrice < "+max+" AND MinPrice > "+ min + ";" );
			//"select * from American WHERE MaxPrice < "+max+" AND MinPrice > "+ min + ";"
			while (myRs.next()){
				ArrayList<String> dbRet = new ArrayList<>();
				dbRet.add(myRs.getString("Maker") + ", " + myRs.getString("Model") + ", " + myRs.getString("MinPrice") + ", " + myRs.getString("MaxPrice")); 
				finalResult.add(dbRet);
				//System.out.println(myRs.getString("Maker") + ", " + myRs.getString("Model") + ", " + myRs.getString("MinPrice") + ", " + myRs.getString("MinPrice")); 
			}
			
			System.out.println("Processing Result ... "); 
		}
		catch(Exception exc){
			exc.printStackTrace(); 
		}
		
		return finalResult;
	}
	
}
