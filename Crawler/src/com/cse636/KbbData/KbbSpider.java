package com.cse636.KbbData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class KbbSpider {
	private static final int MAX_PAGES_TO_SEARCH = 10;
    private Set<String> pagesVisited = new HashSet<String>();
    private List<String> pagesToVisit = new LinkedList<String>();
    Map<String, List<Map<String,String>> >list =new HashMap();
    List<String> urlList=new ArrayList();
    /**
     * @param url
     * @param word
     * */
    
    public List<List<String>> carModelList( List<CarInfo> dbResult){
    	List<List<String>> modelsList= new ArrayList();
    	for(CarInfo car : dbResult){
    		String url="http://www.kbb.com/" + car.getMake()+ "/" + car.getModel() +"/" + "2016/styles/?intent=buy-new&bodystyle=sedan";
    		urlList.add(url);
    		
    	}
    	return modelsList;
    }
    
    public List<RetCarInfo> search(String url)
    {
    		List<RetCarInfo> retCarInfoList=new ArrayList();
            String currentUrl;
            SpiderLeg leg = new SpiderLeg();
            if(this.pagesToVisit.isEmpty())
            {
                currentUrl = url;
                this.pagesVisited.add(url);
            }
            else
            {
                currentUrl = this.nextUrl();
            }
            List<KbbObj> tmpList=leg.crawl(currentUrl); 
            for(KbbObj o : tmpList){
            retCarInfoList.add(leg.CarForPrice(o.getCarUrl()));	
            }
            return retCarInfoList;
    }
    
    
    private String nextUrl()
    {
        String nextUrl;
        do
        {
            nextUrl = this.pagesToVisit.remove(0);
        } while(this.pagesVisited.contains(nextUrl));
        this.pagesVisited.add(nextUrl);
        return nextUrl;
    }
    
}
