package com.cse636.KbbData;

public class RetCarInfo {
	String Make;
	String model;
	String option; // 排量
	Integer fairPrice;
	Integer totalPrice;
	String url;
	String review; 
	
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public String getMake() {
		return Make;
	}
	public void setMake(String make) {
		Make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
	public Integer getFairPrice() {
		return fairPrice;
	}
	public void setFairPrice(Integer fairPrice) {
		this.fairPrice = fairPrice;
	}
	public Integer getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
