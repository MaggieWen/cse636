package com.cse636.KbbData;

public class KbbObj {
	String carType;
	String carUrl;
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	public String getCarUrl() {
		return carUrl;
	}
	public void setCarUrl(String carUrl) {
		this.carUrl = carUrl;
	}
	public KbbObj(String carType, String carUrl) {
		super();
		this.carType = carType;
		this.carUrl = carUrl;
	}
	
}
