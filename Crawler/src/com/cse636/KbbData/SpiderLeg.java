package com.cse636.KbbData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SpiderLeg {
	private static final String USER_AGENT =
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
	private List<Map<String, String>> links = new LinkedList<>(); // Just a list of URLs
	private Document htmlDocument; // This is our web page, or in other words, our document
	
	
	public List<KbbObj> crawl(String url){
			List<KbbObj> kbbList=new ArrayList();
		  try
	        {
			  
	            Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
	            Document htmlDocument = connection.get();
	            this.htmlDocument = htmlDocument;
	        }
	        catch(IOException ioe)
	        {
	            // We were not successful in our HTTP request
	            System.out.println("Error in out HTTP request " + ioe);
	        }
//		  System.out.println("Received web page at " + url);
//          String title = htmlDocument.title();  
//          System.out.println("title is: " + title);  
     
          Elements cars=htmlDocument.getElementsByClass("vehicle-styles-head accordion-sub expandable");
        
          System.out.println(cars.size());
          for(Element link : cars)
          {
          	String carType=link.select("div > div").get(0).text();
          	String carUrl="http://www.kbb.com"+link.select("a[href]").attr("href");
          	carUrl=carUrl.replace("/options", "");
            System.out.println( link.select("div > div").get(0).text());
            System.out.println(link.select("a[href]").attr("href") );
            kbbList.add(new KbbObj(carType, carUrl) );
          }
          	return kbbList;
	} // Give it a URL and it makes an HTTP request for a web page
	
	public RetCarInfo CarForPrice(String url){
		 try
	        {
			  
	            Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
	            Document htmlDocument = connection.get();
	            this.htmlDocument = htmlDocument;
	        }
	        catch(IOException ioe)
	        {
	            // We were not successful in our HTTP request
	            System.out.println("Error in out HTTP request " + ioe);
	        }
		 	RetCarInfo rc=new RetCarInfo();
		 	
//		 	<tr class="stripe bg-d">
//	        <td>Total Fair Purchase Price</td>
//	        <td>$28,706</td>
//	      </tr>
		 	 Elements cars=htmlDocument.getElementsByClass("stripe bg-d");
		 	 String fairprice=cars.get(0).select("td").get(1).text().replace("$","" );
		 	 rc.setFairPrice(priceToInt(fairprice) );
		 	 System.out.println(rc.getFairPrice() );
		 	 
		 	String totalprice=cars.get(1).select("td").get(1).text().replace("$","" );
		 	 rc.setTotalPrice(priceToInt(totalprice) );
		 	 
		 	 return rc;
        }
	
	
	private int priceToInt(String price){
		String[] ps=price.split(",");
		int ret=0;
		ret+=Integer.valueOf(ps[0])*1000;
		ret+=Integer.valueOf(ps[1]);
		return ret;
	}
	
	
	
}
