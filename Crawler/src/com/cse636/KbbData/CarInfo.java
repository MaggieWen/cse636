package com.cse636.KbbData;

public class CarInfo {
	private String make;
	private String model;
	private Integer estimatePrice; //url,rate
	//review of list
	
	public CarInfo(String make, String model) {
		super();
		this.make = make;
		this.model = model;
	}
	public CarInfo(String make, String model, Integer estimatePrice) {
		super();
		this.make = make;
		this.model = model;
		this.estimatePrice = estimatePrice;
	}
	public Integer getEstimatePrice() {
		return estimatePrice;
	}
	public void setEstimatePrice(Integer estimatePrice) {
		this.estimatePrice = estimatePrice;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
}
